#!/bin/bash

# Check for input image.
if [[ $1 == "" ]]
then
	echo "Must give input image!"

	exit 1
fi

# Converting to uncompressed image.
convert $1 -strip in.ppm

# Dimensions of image.
x=$(cat in.ppm | head -n 2 | tail -n 1 | awk '{print $1}')
y=$(cat in.ppm | head -n 2 | tail -n 1 | awk '{print $2}')

# Removing the uncompressed image header.
xxd -p in.ppm | fold -w 2 | tail -n $(($x * $y)) | xxd -r -p > out.bin

# Loop counter.
a=1

# Main loop.
for i in $(xxd -p -c 3 out.bin)
do
	echo -n "[color=#${i,,}]█[/color]"

	if [[ $(($a % $x)) == 0 ]]
	then
		echo ""
	fi

	a=$(($a + 1))
done

rm -f in.ppm out.bin

echo ""

exit 0
