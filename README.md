## About

A simple script to turn images into BBCode.

## Requirements

`convert` (Imagemagick)

## Usage

```bash
./img2bbcode.sh <input_image>
```

The program prints the code to stdout by default.

Use a redirect (`>`) to put it into another file.

Like this:

```bash
./img2bbcode.sh <input_image> > out.txt
```
